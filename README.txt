CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Installation
  * Maintainers


INTRODUCTION
------------
Fixes "CAPTCHA session reuse attack detected" messages on certain AJAX-submitted
fields (currently the File module's Upload field).

When AJAX-enabled forms submit fields (such as the File module's AJAX Upload
button), the form is rebuilt and the CAPTCHA component marks the ID of the
CAPTCHA input on the form as having been used.  This module prevents the CAPTCHA
component from being rendered (on the server side - it's never seen by site
visitors anyway) for the AJAX form submission's rebuild process.

Currently only the File module's Upload field is supported, but future support
should be easy to implement.  File a Support Request issue in this module's
queue with the module and field you would like supported, and I'll add it as
quickly as possible.


INSTALLATION
------------
Follow the standard instructions at http://drupal.org/documentation/install/modules-themes
to install the module.  No configuration is necessary.


MAINTAINERS
-----------
- jay.dansand (Jay Dansand)
